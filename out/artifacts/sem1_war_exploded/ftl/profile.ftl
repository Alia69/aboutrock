<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Profile</title>
    <link href="../css/style.css" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
</head>
<body>
<#include "navbar.ftl" parse=true>
<div class="profile">
    <h1>${Request.username}</h1>
    <h2>${Request.name}</h2>
    <h4>${Request.about}</h4>
</div>

</body>
</html>