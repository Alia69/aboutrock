<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Bands</title>
    <link href="../css/style.css" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap-theme.min.css">
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
</head>
<body>

<#include "navbar.ftl" parse=true>

<div class="search">
    <form>
        <input type="text" placeholder="Search band">
        <button type="submit"></button>
    </form>
</div>


<div class="artists_0">
    <a href="#">
        <div class="row">
            <div class="col-md-3"></div>
            <div class="col-md-2">
                <div class="table">
                </div>
            </div>
            <div class="col-md-3">
                <h2>Bring Me The Horizon</h2>
                <p>deathcore metalcore</p>
            </div>

            <div class="col-md-4"></div>
        </div>
    </a>
</div>

</body>
</html>