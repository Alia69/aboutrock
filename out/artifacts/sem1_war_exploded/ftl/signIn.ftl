<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Sign in</title>
    <link href="../css/style.css" rel="stylesheet">
</head>
<body>
<#include "navbar.ftl" parse=true>

<div class="container">
    <form class="form-signin" action="/Login" role="form" method="post">
        <h1>Sign in</h1>
        <input type="text" class="form-control" name = "username" placeholder="username" required autofocus>
        <input type="password" class="form-control" name= "password" placeholder="Password" required autofocus>
        <label class="checkbox">
            <input type="checkbox" name = "checkbox" value="remember-me"> Remember me
        </label>
        <br />
        <button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
        <div id="err">
            <#if fail>
                <script>alert("Wrong login or password")</script>
            </#if>
        </div>
    </form>
</div>
</body>
</html>