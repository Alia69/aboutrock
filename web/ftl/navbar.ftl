<div class="navigation">
    <ul>
        <li><a href="/">News</a></li>
        <li><a href="/bands">Bands</a></li>
        <#if login>
            <li><a href="/Login">Sign in</a></li>
            <li><a href="/SignUp">Sign up</a></li>
        <#else>
            <li><a href="/profile">Profile</a></li>
            <li><a href="/logout"> Logout</a></li>
        </#if>
    </ul>
</div>