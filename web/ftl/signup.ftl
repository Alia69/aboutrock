<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Sign up</title>
    <link href="../css/style.css" rel="stylesheet">
</head>
<body>
<#include "navbar.ftl" parse=true>

<div class="sign_up">
    <form class="form-signup" role="form" action="/SignUp" method="post">
        <h2 class="form-signup-heading">Sign up</h2>
        <input type="text" class="form-control" name = "name" placeholder="Name" required autofocus>
        <input type="text" class="form-control" name = "username" placeholder="username" required autofocus>
        <input type="password" class="form-control" name = "password" placeholder="Password" required autofocus>
        <input type="password" class="form-control" name = "repeat_password" placeholder="Confirm password" required autofocus>
        <input type="text" class="form-control" name = "about" placeholder="About me" required autofocus>
        <button class="btn btn-lg btn-primary btn-block" type="submit">Sign up</button>
    </form>
</div>
</body>
</html>