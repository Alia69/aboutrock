package database.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DBConnection {
    private static Connection conn;

    public static Connection getDBConnection(){
        if (conn == null) {
            try {
                Class.forName("org.postgresql.Driver");
                conn = DriverManager.getConnection(
                        "jdbc:postgresql://localhost:5432/Project",
                        "postgres", "postgres");
                } catch (SQLException | ClassNotFoundException e) {
                    e.printStackTrace();
                }
        }
        return conn;
    }

}
