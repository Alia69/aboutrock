package database.sql;

import database.dao.AbsractDAO;
import model.New;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class NewDAO extends AbsractDAO<New> {
    public NewDAO(Connection connection) {
        super(connection);
    }

    @Override
    public String getSelectQuery() {
        return "SELECT * FROM new;";
    }

    @Override
    public String getInsertQuery() {
        return "INSERT INTO new(title, content, photo_url) VALUES(?,?,?);";
    }

    @Override
    public String getUpdateQuery() {
        return ("UPDATE new" +
                "SET title = ?, content = ?, photo_url = ?" +
                "WHERE id = ?;"
        );
    }

    @Override
    public String getDeleteQuery() {
        return "DELETE FROM new WHERE id = ?;";
    }

    @Override
    protected List<New> parseResultSet(ResultSet resultSet) {
        List<New> result = new ArrayList<>();
        try {
            while (resultSet.next()) {
                int id = resultSet.getInt("id");
                String title = resultSet.getString("title");
                String content = resultSet.getString("content");
                String photo_url = resultSet.getString("photo_url");
                New new_1 = new New(id, title, content, photo_url);
                result.add(new_1);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }

    @Override
    protected void prepareStatementForInsert(PreparedStatement statement, New object) {
        try {
            statement.setString(1, object.getTitle());
            statement.setString(2, object.getContent());
            statement.setString(3, object.getPhoto_url());
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    @Override
    protected void prepareStatementForUpdate(PreparedStatement statement, New object)  {
        try {
            prepareStatementForInsert(statement, object);
            statement.setInt(4, object.getId());
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
