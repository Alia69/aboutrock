package database.sql;

import database.dao.AbsractDAO;
import model.Genre;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class GenreDAO extends AbsractDAO<Genre>{
    public GenreDAO(Connection connection) {
        super(connection);
    }

    @Override
    public String getSelectQuery() {
        return "SELECT * FROM genre;";
    }

    @Override
    public String getInsertQuery() {
        return "INSERT INTO genre(\"name\") VALUES(?);";
    }

    @Override
    public String getUpdateQuery() {
        return "UPDATE genre SET \"name\" = ? WHERE id = ?;";
    }

    @Override
    public String getDeleteQuery() {
        return "DELETE FROM genre WHERE id = ?; ";
    }

    @Override
    protected List<Genre> parseResultSet(ResultSet resultSet) {
        List<Genre> result = new ArrayList<>();
        try {
            while (resultSet.next()) {
                int id = resultSet.getInt("id");
                String name = resultSet.getString("name");
                Genre genre = new Genre(id, name);
                result.add(genre);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }

    @Override
    protected void prepareStatementForInsert(PreparedStatement statement, Genre object) {
        try {
            statement.setString(1,object.getName());
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void prepareStatementForUpdate(PreparedStatement statement, Genre object) {
        try {
            prepareStatementForInsert(statement, object);
            statement.setInt(4, object.getId());
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
