package database.sql;

import database.dao.AbsractDAO;
import model.Band_genre;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class Band_genreDAO extends AbsractDAO<Band_genre> {
    public Band_genreDAO(Connection connection) {
        super(connection);
    }

    @Override
    public String getSelectQuery() {
        return "SELECT * FROM band_genre;";
    }

    @Override
    public String getInsertQuery() {
        return "INSERT INTO band_genre(band_id,genre_id) VALUES(?,?);";
    }

    @Override
    public String getUpdateQuery() {
        return "UPDATE band_genre SET band_id = ?, genre_id = ? WHERE id = ?;";
    }

    @Override
    public String getDeleteQuery() {
        return "DELETE FROM band_genre WHERE id = ?;";
    }

    @Override
    protected List<Band_genre> parseResultSet(ResultSet resultSet) {
        List<Band_genre> result = new ArrayList<>();
        try {
            while (resultSet.next()) {
                int id = resultSet.getInt("id");
                int band_id = resultSet.getInt("band_id");
                int genre_id = resultSet.getInt("genre_id");
                Band_genre band_genre = new Band_genre(id, band_id, genre_id);
                result.add(band_genre);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }

    @Override
    protected void prepareStatementForInsert(PreparedStatement statement, Band_genre object) {
        try {
            statement.setInt(1, object.getBand_id());
            statement.setInt(2, object.getGenre_id());
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void prepareStatementForUpdate(PreparedStatement statement, Band_genre object){
        try {
            prepareStatementForInsert(statement, object);
            statement.setInt(3,object.getId());
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
