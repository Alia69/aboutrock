package database.sql;

import database.dao.AbsractDAO;
import model.Video;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class VideoDAO extends AbsractDAO<Video> {
    public VideoDAO(Connection connection) {
        super(connection);
    }

    @Override
    public String getSelectQuery() {
        return "SELECT * FROM video;";
    }

    @Override
    public String getInsertQuery() {
        return "INSERT INTO video(band_id, song_id, comtent_url) VALUES(?,?,?);";
    }

    @Override
    public String getUpdateQuery() {
        return "UPDATE video" +
                "SET band_id = ?, song_id = ?, comtent_url = ?" +
                "WHERE id = ?;";
    }

    @Override
    public String getDeleteQuery() {
        return "DELETE FROM video WHERE id = ?;";
    }

    @Override
    protected List<Video> parseResultSet(ResultSet resultSet) {
        List<Video> result = new ArrayList<>();
        try {
            while (resultSet.next()) {
                int id = resultSet.getInt("id");
                int band_id = resultSet.getInt("band_id");
                int song_id = resultSet.getInt("song_id");
                String content = resultSet.getString("comtent_url");
                Video video = new Video(id, band_id, song_id,content);
                result.add(video);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }

    @Override
    protected void prepareStatementForInsert(PreparedStatement statement, Video object) {
        try {
            statement.setInt(1,object.getBand_id());
            statement.setInt(2,object.getSong_id());
            statement.setString(3,object.getComtent_url());
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void prepareStatementForUpdate(PreparedStatement statement, Video object) {
        try {
            prepareStatementForInsert(statement, object);
            statement.setInt(4,object.getId());
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
