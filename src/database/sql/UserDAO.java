package database.sql;

import database.dao.AbsractDAO;
import model.User;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class UserDAO extends AbsractDAO<User> {

    public UserDAO(Connection connection) {
        super(connection);
    }

    @Override
    public String getSelectQuery() {
        return "SELECT * FROM \"user\"";
    }

    @Override
    public String getInsertQuery() {
        return "INSERT INTO \"user\"(username, \"name\", about, password) VALUES(?,?,?,?)";
    }

    @Override
    public String getUpdateQuery() {
        return "UPDATE \"user\" " +
                "SET username = ?, \"name\" = ?, about = ?, password = ?" +
                "WHERE id = ?";
    }

    @Override
    public String getDeleteQuery() {
        return "DELETE FROM \"user\" WHERE id = ?;";
    }

    @Override
    protected List<User> parseResultSet(ResultSet resultSet) {
        List<User> result = new ArrayList<>();
        try {
            while (resultSet.next()) {
                int id = resultSet.getInt("id");
                String username = resultSet.getString("username");
                String name = resultSet.getString("name");
                String about = resultSet.getString("about");
                String password = resultSet.getString("password");
                User user = new User(username, name, about, password);
                result.add(user);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }

    @Override
    protected void prepareStatementForInsert(PreparedStatement statement, User object) {
        try {
            statement.setString(1, object.getUsername());
            statement.setString(2, object.getName());
            statement.setString(3, object.getAbout());
            statement.setString(4, object.getPassword());
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void prepareStatementForUpdate(PreparedStatement statement, User object) {
        try {
            prepareStatementForInsert(statement, object);
            statement.setInt(5, object.getId());
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public User getUserByUsername(String username) {
        List<User> list = new ArrayList<>();
        try {
            PreparedStatement statement = connection.prepareStatement(getSelectQuery()
                    + " WHERE username = ?");
            statement.setString(1, username);
            ResultSet resultSet = statement.executeQuery();
            list = parseResultSet(resultSet);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        if (list.size() == 0) {
            return null;
        }
        return list.iterator().next();
    }

    public User getUserByName(String name) {
        List<User> list = new ArrayList<>();
        try {
            PreparedStatement statement = connection.prepareStatement(getSelectQuery()
                    + " WHERE name = ?");
            statement.setString(2, name);
            ResultSet resultSet = statement.executeQuery();
            list = parseResultSet(resultSet);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        if (list.size() == 0) {
            return null;
        }
        return list.iterator().next();
    }
}

