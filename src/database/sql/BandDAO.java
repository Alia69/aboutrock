package database.sql;

import database.dao.AbsractDAO;
import model.Band;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class BandDAO extends AbsractDAO<Band> {
    public BandDAO(Connection connection) {
        super(connection);
    }

    @Override
    public String getSelectQuery() {
        return "SELECT * FROM band;";
    }

    @Override
    public String getInsertQuery() {
        return "INSERT INTO band (\"name\",about) VALUES(?,?);";
    }

    @Override
    public String getUpdateQuery() {
        return "UPDATE topic SET \"name\" = ?, about= ? WHERE id = ?;";
    }

    @Override
    public String getDeleteQuery() {
        return "DELETE FROM band WHERE id=?;";
    }

    @Override
    protected List<Band> parseResultSet(ResultSet resultSet) {
        List<Band> result = new ArrayList<>();
        try {
            while (resultSet.next()) {
                int id = resultSet.getInt("id");
                String name = resultSet.getString("name");
                String about = resultSet.getString("about");
                Band band = new Band(id, name, about);
                result.add(band);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }

    @Override
    protected void prepareStatementForInsert(PreparedStatement statement, Band object) {
        try {
            statement.setString(1, object.getName());
            statement.setString(2, object.getAbout());
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void prepareStatementForUpdate(PreparedStatement statement, Band object) {
        try {
            prepareStatementForInsert(statement, object);
            statement.setInt(3,object.getId());
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
