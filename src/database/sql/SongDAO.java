package database.sql;

import database.dao.AbsractDAO;
import model.Song;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class SongDAO extends AbsractDAO<Song> {
    public SongDAO(Connection connection) {
        super(connection);
    }

    @Override
    public String getSelectQuery() {
        return "SELECT * FROM song;";
    }

    @Override
    public String getInsertQuery() {
        return "INSERT INTO song(band_id, \"name\", lyrics) VALUES(?,?,?);";
    }

    @Override
    public String getUpdateQuery() {
        return "UPDATE song" +
                "SET band_id = ?, \"name\" = ?, lyrics = ?" +
                "WHERE id = ?;";
    }

    @Override
    public String getDeleteQuery() {
        return "DELETE FROM song WHERE id = ?;";
    }

    @Override
    protected List<Song> parseResultSet(ResultSet resultSet) {
        List<Song> result = new ArrayList<>();
        try {
            while (resultSet.next()) {
                int id = resultSet.getInt("id");
                int band_id = resultSet.getInt("band_id");
                String name = resultSet.getString("\"name\"");
                String lyrics = resultSet.getString("lyrics");
                Song song = new Song(id, band_id, name, lyrics);
                result.add(song);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }

    @Override
    protected void prepareStatementForInsert(PreparedStatement statement, Song object) {
        try {
            statement.setInt(1, object.getBand_id());
            statement.setString(2,object.getName());
            statement.setString(3,object.getLyrics());
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void prepareStatementForUpdate(PreparedStatement statement, Song object){
        prepareStatementForInsert(statement, object);
        try {
            statement.setInt(4,object.getId());
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
