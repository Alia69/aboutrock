package servlets;

import database.dao.DBConnection;
import database.sql.UserDAO;
import model.User;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import java.io.File;
import java.io.IOException;
import java.util.Random;
import java.util.regex.Pattern;

public class ProfileServlet extends HttpServlet {

    private final static String COOKIE_USER = "user_id";
    private final static String SESSION_USER = "current_user";

    private boolean checkNickname(String username) {
        UserDAO userDao = new UserDAO(DBConnection.getDBConnection());
        return userDao.getUserByUsername(username) == null;
    }

    private void openProfile(HttpServletRequest request, User user, HttpServletResponse response) throws ServletException, IOException {
        request.setAttribute("username", user.getUsername());
        request.setAttribute("name", user.getName());
        request.setAttribute("about", user.getAbout());

        if (request.getSession().getAttribute(SESSION_USER) != null) {
            request.setAttribute("login", false);
        } else request.setAttribute("login", true);
        request.setAttribute("current_url", request.getRequestURI());
        request.getRequestDispatcher("profile.ftl").forward(request, response);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

            String username = request.getParameter("username");
            String name = request.getParameter("name");
            String about = request.getParameter("about");
            User user = ServletHelper.getUserFromDAO(request);

            openProfile(request, user, response);

        }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        ServletHelper.initAttributes(request);
        User user = ServletHelper.getUserFromDAO(request);
        openProfile(request, user, response);
        request.getRequestDispatcher("/profile.ftl").forward(request, response);
    }
}
