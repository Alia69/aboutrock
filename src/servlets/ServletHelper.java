package servlets;

import database.dao.DBConnection;
import database.sql.UserDAO;
import model.User;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class ServletHelper {

    private final static String COOKIE_USER = "user_id";
    private final static String SESSION_USER = "current_user";

    public static String getLogin(HttpServletRequest request){
        Cookie[] cookies = request.getCookies();
        String login = "";
        if (cookies != null){
            for (Cookie cookie : cookies){
                switch (cookie.getName()){
                    case COOKIE_USER:
                        login = cookie.getValue();
                        break;
                }
            }
        }
        return login;
    }

    public static void initCookie(HttpServletRequest request){
        Cookie [] cookies = request.getCookies();
        if (cookies != null){
            for (Cookie cookie : cookies){
                switch (cookie.getName()){
                    case COOKIE_USER:
                        request.getSession().setAttribute(SESSION_USER, cookie.getValue());
                        break;
                }
            }
        }
    }

    public static void initAttributes(HttpServletRequest request){

        request.setAttribute("current_url", request.getRequestURI());

        if (request.getSession().getAttribute(SESSION_USER) != null)
            request.setAttribute("login", false);
        else request.setAttribute("login", true);
    }

    public static User getUserFromDAO(HttpServletRequest request) {
        UserDAO userDao = new UserDAO(DBConnection.getDBConnection());
        return userDao.getUserByUsername(getLogin(request));
    }
}

