package servlets;

import database.dao.DBConnection;
import database.sql.UserDAO;
import model.User;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class SignupServlet extends HttpServlet {

    private UserDAO userDao;
    private final static String COOKIE_USER = "user_id";
    private final static String SESSION_USER = "current_user";

    private boolean checkLogin(String username) {
        userDao = new UserDAO(DBConnection.getDBConnection());
        return userDao.getUserByUsername(username) == null;
    }


    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String name = request.getParameter("name");
        String username = request.getParameter("username");
        String password = request.getParameter("password");
        String repeatPassword = request.getParameter("repeat_password");
        String about = request.getParameter("about");

        User user = new User(name, username, password, about);
        userDao.insert(user);
        response.sendRedirect("/login");


    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        ServletHelper.initAttributes(request);

        request.getRequestDispatcher("/signup.ftl").forward(request, response);
    }
}
