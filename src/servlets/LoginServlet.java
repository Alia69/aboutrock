package servlets;

import database.dao.DBConnection;
import database.sql.UserDAO;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.security.NoSuchAlgorithmException;

public class LoginServlet extends HttpServlet {

    private final String COOKIE_USER = "user_id";
    private final String SESSION_USER = "current_user";

    private boolean check(String username, String password){
        UserDAO dao = new UserDAO(DBConnection.getDBConnection());
        String DBPassword = dao.getUserByUsername(username) == null ? null : dao.getUserByUsername(username).getPassword();
        return password.equals(DBPassword);
    }



    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String username = request.getParameter("username");
        String password = request.getParameter("password");

        boolean remember = request.getParameter("checkbox") != null;

        // проверка, что username существует и пароль ему соответствует
        if (check(username, password)){
            request.getSession().setAttribute(SESSION_USER, username);
            Cookie cookie = new Cookie(COOKIE_USER, username);
            if (remember){
                cookie.setMaxAge(15 * 24 * 60 * 60);
            }
            else {
                cookie.setMaxAge(-1);
            }
            response.addCookie(cookie);
            response.sendRedirect("/");
        }
        else {
            request.setAttribute("fail", true);
            request.setAttribute("login",true);
            request.getRequestDispatcher("/signIn.ftl").forward(request, response);
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        ServletHelper.initAttributes(request);;
        request.getRequestDispatcher("/signIn.ftl").forward(request, response);
    }
}
