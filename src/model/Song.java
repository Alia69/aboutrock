package model;

import database.dao.Identified;

public class Song implements Identified{
    private int id;
    private int band_id;
    private String name;
    private String lyrics;

    public Song(int id, int band_id, String name, String lyrics) {
        this.id = id;
        this.band_id = band_id;
        this.name = name;
        this.lyrics = lyrics;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getBand_id() {
        return band_id;
    }

    public void setBand_id(int band_id) {
        this.band_id = band_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLyrics() {
        return lyrics;
    }

    public void setLyrics(String lyrics) {
        this.lyrics = lyrics;
    }
}
