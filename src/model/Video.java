package model;

import database.dao.Identified;

public class Video implements Identified {
    private int id;
    private int band_id;
    private int song_id;
    private String comtent_url;

    public Video(int id, int band_id, int song_id, String comtent_url) {
        this.id = id;
        this.band_id = band_id;
        this.song_id = song_id;
        this.comtent_url = comtent_url;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getBand_id() {
        return band_id;
    }

    public void setBand_id(int band_id) {
        this.band_id = band_id;
    }

    public int getSong_id() {
        return song_id;
    }

    public void setSong_id(int song_id) {
        this.song_id = song_id;
    }

    public String getComtent_url() {
        return comtent_url;
    }

    public void setComtent_url(String comtent_url) {
        this.comtent_url = comtent_url;
    }
}
