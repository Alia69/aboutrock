package model;

import database.dao.Identified;

public class Band_genre implements Identified{
    private int id;
    private int band_id;
    private int genre_id;

    public Band_genre(int id, int band_id, int genre_id) {
        this.id = id;
        this.band_id = band_id;
        this.genre_id = genre_id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getBand_id() {
        return band_id;
    }

    public void setBand_id(int band_id) {
        this.band_id = band_id;
    }

    public int getGenre_id() {
        return genre_id;
    }

    public void setGenre_id(int genre_id) {
        this.genre_id = genre_id;
    }
}
