package model;
import database.dao.Identified;

public class User implements Identified {
    private int id;
    private String username;
    private String name;
    private String about;
    private String password;

    public User( String username, String name, String about, String password) {
        this.username = username;
        this.name = name;
        this.about = about;
        this.password = password;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAbout() {
        return about;
    }

    public void setAbout(String about) {
        this.about = about;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
