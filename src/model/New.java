package model;

import database.dao.Identified;

public class New implements Identified {
    private int id;
    private String title;
    private String content;
    private String photo_url;

    public New(int id, String title, String content, String photo_url) {
        this.id = id;
        this.title = title;
        this.content = content;
        this.photo_url = photo_url;
    }

    @Override
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getPhoto_url() {
        return photo_url;
    }

    public void setPhoto_url(String photo_url) {
        this.photo_url = photo_url;
    }
}
